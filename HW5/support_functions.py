# File containing functions used in main HW script
import numpy as np
from typing import List, Union

realnum = Union[float, int]


def Klocmaker(dcosines: List[realnum],
              E: realnum = 1,
              A: realnum = 1,
              L: realnum = 1) -> np.ndarray:
    # Make sure the inputs are good
    assert len(dcosines) in (2, 3), 'There are not enough or too many direction cosine arguments.'

    # Generate the local stiffness matrix based on the 
    if len(dcosines) == 2:
        cx, cy = dcosines[0:2]
        Kloc = E*A/L*np.array([[cx ** 2, cy * cx, -cx ** 2, -cy * cx],
                               [cy * cx, cy ** 2, -cy * cx, -cy ** 2],
                               [-cx ** 2, -cy * cx, cx ** 2, cy * cx],
                               [-cy * cx, -cy ** 2, cy * cx, cy ** 2]])
    elif len(dcosines) == 3:
        cx, cy, cz = dcosines[0:3]
        Kloc = E*A/L*np.array([[cx * cx, cx * cy, cx * cz, -cx * cx, -cx * cy, -cx * cz],
                               [cy * cx, cy * cy, cy * cz, -cy * cx, -cy * cy, -cy * cz],
                               [cz * cx, cz * cy, cz * cz, -cz * cx, -cz * cy, -cz * cz],
                               [-cx * cx, -cx * cy, -cx * cz, cx * cx, cx * cy, cx * cz],
                               [-cy * cx, -cy * cy, -cy * cz, cy * cx, cy * cy, cy * cz],
                               [-cz * cx, -cz * cy, -cz * cz, cz * cx, cz * cy, cz * cz]])
    return Kloc
