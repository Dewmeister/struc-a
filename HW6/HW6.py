# Comp. Methods for Struc. A. HW #6:
# Dewey Melton, dtm862
# 2-D beam solver for nodal displacements and member strains/stresses/loading

import numpy as np

# ============================================== Settings and Config. ==================================================
# Define how many dimensions the frame will be analyzed in
# WARNING - NOT YET UPDATED FOR 3D
dims = 2

# Set up names of files so that we can easily switch between frames
filenames = {'nodes': 'nodes', 'elements': 'elements', 'displacements': 'displacements', 'forces': 'forces'}
prefix, suffix, usefixes = 'HW6', '', 1

if usefixes == 1:
    for refname, name in filenames.items():
        filenames[refname] = prefix + name + suffix

# Set up names for pretty printed output: units, dof names, etc.
dofnames = ['(x-direction)', '(y-direction)', '(z-axis rotation)']
units = {'displacement': 'm', 'force': 'N', 'pressure': 'Pa', 'area': 'm^2', 'stress': 'N/m^2', 'strain': 'm/m'}
ratios = {'displacement': 'PL/EA', 'force': 'P', 'pressure': 'P/A', 'area': 'A', 'stress': 'P/A', 'strain': 'P/EA'}


# Choose which parts of the output to print
printoptions = {'ratiometric': 1, 'displacements': 1, 'forces': 1, 'strains': 1, 'stresses': 1, 'Pcrit': 1, 'Pyield': 1}
# ======================================================================================================================
# Load in nodes and nodal positions
nodefile = np.loadtxt(filenames['nodes'], skiprows=1, ndmin=2)
ncoords = nodefile[:, 1:(dims + 1)]
nnodes = nodefile.shape[0]
# print('Nodes:')
# print(nodes)

# Make sure the node inputs match the assumed # of dimensions the script will try to analyze
assert ncoords.shape[1] == dims, "The number of coordinates given per node disagrees with the script's dimensionality"

# Load in elements
elementfile = np.loadtxt(filenames['elements'], skiprows=1, ndmin=2)
elenodes = np.array(elementfile[:, 1:(dims + 1)], dtype=int)
neles = elementfile.shape[0]
if elementfile.shape[1] > dims + 1:
    Eele = elementfile[:, [dims + 1]]  # take first post-node column of input file as providing E for each member
    Aele = elementfile[:, [(dims + 2)]]  # take second post-node column of input file as providing Acs for each member
    Iele = elementfile[:, [(dims + 3)]]  # take third post-node column of input as providing I for each member
else:
    Aele = np.ones([neles, 1])
    Eele = np.ones([neles, 1])
    Iele = np.ones([neles, 1])
# print('Elements:')
# print(elements)
# if elementfile.shape[1] > (dims + 1):
#     print('Moduli of Elasticity:')
#     print(Eele)
#     print('Cross-Sectional Areas:')
#     print(Aele)

# Load in boundary conditions
dispfile = np.loadtxt(filenames['displacements'], skiprows=1, ndmin=2)
disps = dispfile[:, 0:]
ndbcs = dispfile.shape[0]
# print('Disps:')
# print(disps)


# Load in known external/applied forces
forcefile = np.loadtxt(filenames['forces'], skiprows=1, ndmin=2)
forces = forcefile[:, 0:]
nforces = forcefile.shape[0]
# print('Forces:')
# print(forces)

# Create gcon array
nodedofs = dims + 1
gcon = np.empty([nnodes, nodedofs], dtype=int)
for node in range(nnodes):
    for dof in range(nodedofs):
        gcon[node, dof] = nodedofs*node + dof

# Apply known disps and rearrange these to bottom of gcon
for bc in disps.tolist():
    # Read in the node # and dof # of the boundary condition
    dbcnode = int(bc[0])
    dbcdof = int(bc[1])

    # Cycle through every element in gcon; move all other entries forwards
    dofi = gcon[dbcnode, dbcdof]  # Get gcon address of current bc
    for idx1 in range(nnodes):  # do i
        for idx2 in range(nodedofs):  # do j
            dofj = gcon[idx1, idx2]  # Get gcon address of the node dof currently being considered
            if dofj > dofi:  # If considered node dof gcon address greater than the address of bc, move it up one
                gcon[idx1, idx2] -= 1

    # Move current bc gcon to end
    gcon[dbcnode, dbcdof] = int(nodedofs*nnodes - 1)  # Psuedo code asks for 2*nnodes; include the -1 to correct for 0 indexing

# Calculate the frame's number of degrees of freedom
ndofs = nodedofs*nnodes - ndbcs

# Initialize global force and displacement vectors
uglob = np.zeros([nnodes*ndofs, 1])
for bc in disps:
    uglob[gcon[int(bc[0]), int(bc[1])]] = bc[2]

fglob = np.zeros([nnodes*ndofs, 1])
for f in forces:
    fglob[gcon[int(f[0]), int(f[1])]] = f[2]

# Generate actual values for reduced stiffness matrix and force vector
# WARNING: NOT YET UPDATED FOR 3D

# generate the projection on to x, y, and z axis for each element (x=projvec[ele#, 0], y=[ele#, 1], and z=[ele#, 2])
projvec = np.empty([neles, dims])
for idx, ele in enumerate(elenodes.tolist()):
    for dim in range(dims):
        lnode0coord = ncoords[ele[0], dim]
        lnode1coord = ncoords[ele[1], dim]
        projvec[idx, dim] = lnode1coord - lnode0coord

# Generate total length and direction cosines of each element
Lvec = np.sqrt(np.sum(projvec**2, axis=1))
dcvec = np.empty([neles, dims])
for idx in range(dims):
    dcvec[:, idx] = projvec[:, idx]/Lvec

# Initialize reduced stiffness matrix and force vector
Kred = np.zeros([ndofs, ndofs])
Fred = fglob[0:ndofs, [0]]
for idx, ele in enumerate(elenodes.tolist()):
    [cx, cy] = dcvec[idx, :]
    L, [E], [A], [I] = Lvec[idx], Eele[idx], Aele[idx], Iele[idx]
    # put in new method of generating Kloc : add R, add K, Kloc = R.T@K@R (I think)
    R = np.array([[cx, cy, 0, 0, 0, 0],
                  [-cy, cx, 0, 0, 0, 0],
                  [0, 0, 1, 0, 0, 0],
                  [0, 0, 0, cx, cy, 0],
                  [0, 0, 0, -cy, cx, 0],
                  [0, 0, 0, 0, 0, 1]])
    K = np.array([[E*A/L, 0, 0, -E*A/L, 0, 0],
                  [0, 12*E*I/L**3, 6*E*I/L**2, 0, -12*E*I/L**3, 6*E*I/L**2],
                  [0, 6*E*I/L**2, 4*E*I/L, 0, -6*E*I/L**2, 2*E*I/L],
                  [-E*A/L, 0, 0, E*A/L, 0, 0],
                  [0, -12*E*I/L**3, -6*E*I/L**2, 0, 12*E*I/L**3, -6*E*I/L**2],
                  [0, 6*E*I/L**2, 2*E*I/L, 0, -6*E*I/L**2, 4*E*I/L]])
    Kloc = R.T@K@R
    for inode in range(2):
        for idof in range(nodedofs):
            ldofi = nodedofs*inode + idof
            gdofi = gcon[ele[inode], idof]
            if gdofi >= ndofs:
                continue
            for jnode in range(2):
                for jdof in range(nodedofs):
                    ldofj = nodedofs*jnode + jdof
                    gdofj = gcon[ele[jnode], jdof]
                    if gdofj >= ndofs:
                        Fred[gdofi, [0]] -= Kloc[ldofi, ldofj]*uglob[gdofj]
                    else:
                        Kred[gdofi, gdofj] += Kloc[ldofi, ldofj]

# Solve for the reduced displacement vector
ured = np.linalg.inv(Kred)@Fred
# Reconstruct global displacement vector
uglob[0:ndofs] = ured
u = np.zeros([nnodes, nodedofs])
for idx1 in range(nnodes):
    for idx2 in range(nodedofs):
        u[idx1, idx2] = uglob[gcon[idx1, idx2]]
# Calculate strain, stress, and force in each element
# WARNING: NOT YET UPDATED FOR 3D
strain = np.empty(neles)
stress = np.empty_like(strain)
Fele = np.empty_like(strain)
dvec = np.empty([neles, dims])
for idx, ele in enumerate(elenodes.tolist()):
    lnode0 = ele[0]
    lnode1 = ele[1]
    for dim in range(dims):
        dvec[idx, dim] = u[lnode1, dim] - u[lnode0, dim]
    strain[idx] = np.dot(dvec[idx, :], dcvec[idx, :])/Lvec[idx]
    stress[idx] = strain[idx]*Eele[idx]
    Fele[idx] = stress[idx]*Aele[idx]
# =============================================== Solution Printing ===================================================
# Displacements:
if printoptions['displacements'] == 1:
    for idx1 in range(nnodes):
        for idx2 in range(nodedofs):
            if u[idx1, idx2] == 0:
                unit = ''
            else:
                unit = units['displacement']
            if printoptions['ratiometric'] == 0:
                ratio = ''
            else:
                ratio = '*' + ratios['displacement']
            print(f'The displacement of node {idx1} in d.o.f. {idx2} {dofnames[idx2]} '
                  f'is {u[idx1, idx2]:.3f}{ratio} {unit}')
    print()

# Forces:
if printoptions['forces'] == 1:
    for idx in range(neles):
        if Fele[idx] == 0:
            unit = ''
        else:
            unit = units['force']
        if printoptions['ratiometric'] == 0:
            ratio = ''
        else:
            ratio = '*' + ratios['force']

        print(f'The internal force in element {idx} is {Fele[idx]:.3f}{ratio} {unit}')
    print()

# Strains:
if printoptions['strains'] == 1:
    for idx in range(neles):
        if strain[idx] == 0:
            unit = ''
        else:
            unit = units['strain']
        if printoptions['ratiometric'] == 0:
            ratio = ''
        else:
            ratio = '*' + ratios['strain']
        print(f'The strain in element {idx} is {strain[idx]:.3f}{ratio} {unit}')
    print()

# Stresses:
if printoptions['stresses'] == 1:
    for idx in range(neles):
        if stress[idx] == 0:
            unit = ''
        else:
            unit = units['stress']
        if printoptions['ratiometric'] == 0:
            ratio = ''
        else:
            ratio = '*' + ratios['stress']
        print(f'The stress in element {idx} is {stress[idx]:.3f}{ratio} {unit}')
    print()

# ====================================== Solve for Critical Loads ======================================================
# Assume for calculation that every element is of solid square cross section, and introduce a value for Iz
if sum(Fele > 0) != len(Fele):
    printoptions['Pcrit'] = 0
    printoptions['Pyield'] = 0
    print('There are elements with zero internal forces, which Pcrit and Pyield calculations will not yet handle')

if printoptions['Pcrit'] == 1:
    compresseles = Fele < 0
    s = np.sqrt(Aele)
    Iz = s**4/12  # This formula based on assumption of square cross sections
    Pcritb = np.pi**2*Eele.ravel()*Iz.ravel()/Lvec**2  # .ravel() flattens Nx1 arrays in to singleton, 1D arrays
    CBLperele = Pcritb/Fele * compresseles
    CBLperele = [np.nan if CBLpl == 0 else abs(CBLpl) for CBLpl in CBLperele]
    CBLidx = int(np.nanargmin(CBLperele))
    CBL = CBLperele[CBLidx]
    print(f'Assuming elements with square cross sections, a load of P = {CBL:.3f} {units["force"]} '
          f'will cause element {CBLidx} to buckle.')
    print()

# Assume for calculation that every element is annealed iron, which has Young's Modulus of 211 GPa and sigmay of 100 Mpa
if printoptions['Pyield'] == 1:
    tensioneles = Fele > 0
    sigmay = 100e6  # from a random table on the internet
    PYieldperele = sigmay*Aele.ravel()/Fele * tensioneles
    PYieldperele = [np.nan if PYpe == 0 else PYpe for PYpe in PYieldperele]
    PYieldidx = int(np.nanargmin(PYieldperele))
    PYield = PYieldperele[PYieldidx]
    print(f'Assuming elements made of annealed iron with yield strength of {sigmay} {units["pressure"]}, a load of '
          f'P = {PYield:.3f} {units["force"]} will cause element {PYieldidx} to yield.')
    print()
# ======================================================================================================================
print(u)
