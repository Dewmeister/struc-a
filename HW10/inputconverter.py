import numpy as np
import os

for filename in os.listdir('inputs'):
    firstrow = np.loadtxt('inputs/' + filename, max_rows=1)
    file = np.loadtxt('inputs/' + filename, skiprows=1, ndmin=2)

    if filename.startswith('nodes'):
        print('nodes: ' + filename)
        file[:, 0] -= 1
        np.savetxt('inputs/' + filename.strip('.txt'), file, header=str(firstrow.tolist()), comments='')

    elif filename.startswith('displacements'):
        print('displacements: ' + filename)
        file[:, 0:2] -= 1
        np.savetxt('inputs/' + filename.strip('.txt'), file, header=str(firstrow.tolist()), comments='')

    elif filename.startswith('elements'):
        print('elements: ' + filename)
        file[:, 0:4] -= 1
        np.savetxt('inputs/' + filename.strip('.txt'), file, header=str(firstrow.tolist()), comments='')

    elif filename.startswith('forces'):
        print('forces: ' + filename)
        file[:, 0:2] -= 1
        np.savetxt('inputs/' + filename.strip('.txt'), file, header=str(firstrow.tolist()), comments='')

    else:
        print('Unrecognized filename: ' + filename)
