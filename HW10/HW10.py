# Comp. Methods for Struc. A. HW #10:
# Dewey Melton, dtm862
# 2-D finite element solver for nodal displacements and member strains/stresses/loading
import numpy as np
import matplotlib.pyplot as plt

# ============================================== Settings and Config. ==================================================
dims = 2
np.set_printoptions(precision=3, suppress=True)

# Set up names of files so that we can easily switch between frames
filetypes = {'nodes': 'nodes', 'elements': 'elements', 'displacements': 'displacements', 'forces': 'forces'}
prefix = 'inputs/'
suffixes = ['6', '12', '24', 'R']
filenames = {}

for refname, name in filetypes.items():
    filenames[refname] = []
    for idx, suffix in enumerate(suffixes):
        filenames[refname].append(prefix + name + suffix)

# Declare some storage vars
xaxSxx = []
yaxSyy = []
xaxSyy = []
yaxSxx = []
xaxpos = []
yaxpos = []

# =============================================== Load Input Files =====================================================
for meshnum in range(len(suffixes)):
    # Load in nodes
    nodefile = np.loadtxt(filenames['nodes'][meshnum], skiprows=1, ndmin=2)
    ncoords = nodefile[:, 1:]
    nnodes = nodefile.shape[0]

    # Load in elements
    elementfile = np.loadtxt(filenames['elements'][meshnum], skiprows=1, ndmin=2)
    elenodes = np.array(elementfile[:, 1:], dtype=int)
    neles = elementfile.shape[0]

    elestats = np.loadtxt(filenames['elements'][meshnum], max_rows=1)
    E = elestats[1]
    nu = elestats[2]

    # Load in boundary conditions
    dispfile = np.loadtxt(filenames['displacements'][meshnum], skiprows=1, ndmin=2)
    disps = dispfile[:, 0:]
    ndbcs = dispfile.shape[0]

    # Load in known external/applied forces
    forcefile = np.loadtxt(filenames['forces'][meshnum], skiprows=1, ndmin=2)
    forces = forcefile[:, 0:]
    nforces = forcefile.shape[0]
    # =================================== Apply General Connectivity Algorithm =========================================
    # Create gcon array
    gcon = np.empty([nnodes, dims], dtype=int)
    for node in range(nnodes):
        for dof in range(dims):
            gcon[node, dof] = dims*node + dof

    # Apply known disps and rearrange these to bottom of gcon
    for bc in disps.tolist():
        # Read in the node # and dof # of the boundary condition
        dbcnode = int(bc[0])
        dbcdof = int(bc[1])

        # Cycle through every element in gcon; move all other entries forwards
        dofi = gcon[dbcnode, dbcdof]  # Get gcon address of current bc
        for idx1 in range(nnodes):  # do i
            for idx2 in range(dims):  # do j
                dofj = gcon[idx1, idx2]  # Get gcon address of the node dof currently being considered
                if dofj > dofi:  # If considered node dof gcon address greater than the address of bc, move it up one
                    gcon[idx1, idx2] -= 1

        # Move current bc gcon to end
        gcon[dbcnode, dbcdof] = int(dims*nnodes - 1)  # Psuedo code uses 2*nnodes; include the -1 to correct for 0 indexing

    # Calculate the frame's number of degrees of freedom
    ndofs = dims*nnodes - ndbcs
    # ======================= Calculate Values for Reduced Stiffness Matrix and Force Vector ===========================
    # Initialize global force and displacement vectors
    uglob = np.zeros([nnodes*ndofs, 1])
    for bc in disps:
        uglob[gcon[int(bc[0]), int(bc[1])]] = bc[2]

    fglob = np.zeros([nnodes*ndofs, 1])
    for f in forces:
        fglob[gcon[int(f[0]), int(f[1])]] = f[2]

    # Initialize reduced stiffness matrix and force vector
    Kred = np.zeros([ndofs, ndofs])
    Fred = fglob[0:ndofs, [0]]
    Blist = []
    Clist = []
    for ele in elenodes.tolist():
        [[x1, y1], [x2, y2], [x3, y3]] = ncoords[ele, :]
        A = 1/2*(x1*y2-x2*y1 + x2*y3-x3*y2 + x3*y1-x1*y3)
        a1, b1, c1 = (x2*y3-x3*y2)/2/A, (y2-y3)/2/A, (x3-x2)/2/A
        a2, b2, c2 = (x3*y1-x1*y3)/2/A, (y3-y1)/2/A, (x1-x3)/2/A
        a3, b3, c3 = (x1*y2-x2*y1)/2/A, (y1-y2)/2/A, (x2-x1)/2/A
        B = np.array([[b1, 0, b2, 0, b3, 0],
                      [0, c1, 0, c2, 0, c3],
                      [c1, b1, c2, b2, c3, b3]])
        C = np.array([[E/(1-nu**2), nu*E/(1-nu**2), 0],
                      [nu*E/(1-nu**2), E/(1-nu**2), 0],
                      [0, 0, E/2/(1+nu)]])
        Kloc = A*B.T@C@B
        Blist.append(B)
        Clist.append(C)
        for inode in range(3):
            for idof in range(dims):
                ldofi = dims*inode + idof
                gdofi = gcon[ele[inode], idof]
                if gdofi >= ndofs:
                    continue
                for jnode in range(3):
                    for jdof in range(dims):
                        ldofj = dims*jnode + jdof
                        gdofj = gcon[ele[jnode], jdof]
                        if gdofj >= ndofs:
                            Fred[gdofi, [0]] -= Kloc[ldofi, ldofj]*uglob[gdofj]
                        else:
                            Kred[gdofi, gdofj] += Kloc[ldofi, ldofj]
    # ============================================ Generate Solutions ==================================================
    # Solve for the reduced displacement vector
    ured = np.linalg.inv(Kred)@Fred

    # Reconstruct global displacement vector
    uglob[0:ndofs] = ured
    u = np.zeros([nnodes, dims])
    for idx1 in range(nnodes):
        for idx2 in range(dims):
            u[idx1, idx2] = uglob[gcon[idx1, idx2]]

    # Calculate strain and stress
    strain = np.empty([neles, 3])
    stress = np.empty_like(strain)
    un = np.empty([6, 1])
    for elenum in range(elenodes.shape[0]):
        for j in range(6):
            un[j, 0] = uglob[gcon[elenodes[elenum, (j + 2) // 2 - 1], (j + 2) % 2]]
        elestrain = (Blist[elenum]@un)
        strain[elenum] = elestrain.T
        stress[elenum] = (Clist[elenum]@elestrain).T

    # Pick out the stresses along the x and y axes and sort into their own special list
    xaxSxx.append([])
    yaxSyy.append([])
    xaxSyy.append([])
    yaxSxx.append([])
    xaxpos.append([])
    yaxpos.append([])
    for idx, ele in enumerate(elenodes.tolist()):
        if sum(ncoords[ele, 1] == 0) == 2:  # "If two of the nodes have y coordinate of 0"
            xaxSxx[-1].append(stress[idx, 0])
            xaxSyy[-1].append(stress[idx, 1])
            xaxpos[-1].append(sum(ncoords[ele, 0]/3))
        if sum(ncoords[ele, 0] == 0) == 2:  # "If two of the nodes have x coordinate of 0"
            yaxSxx[-1].append(stress[idx, 0])
            yaxSyy[-1].append(stress[idx, 1])
            yaxpos[-1].append(sum(ncoords[ele, 1]/3))

    # ============================================== Post-Processing ===================================================
    # Plot the undeformed and deformed meshgrids together
    scale = 0.05
    deformednodes = ncoords + scale*u
    meshfig, meshax = plt.subplots()

    x, y = ncoords[:, 0], ncoords[:, 1]
    meshax.scatter(x, y, marker='.', color='k', s=50, label='Undeformed')

    x, y = deformednodes[:, 0], deformednodes[:, 1]
    meshax.scatter(x, y, marker='.', color='r', s=50, label='Deformed')

    for ele in elenodes.tolist():
        lnode0 = ele[0]
        lnode1 = ele[1]
        lnode2 = ele[2]
        xnorm, ynorm = zip(ncoords[lnode0, :], ncoords[lnode1, :], ncoords[lnode2, :], ncoords[lnode0, :])
        xdef, ydef = zip(deformednodes[lnode0, :], deformednodes[lnode1, :], deformednodes[lnode2, :], deformednodes[lnode0, :])
        meshax.plot(xnorm, ynorm, color='k', linewidth=0.5)
        meshax.plot(xdef, ydef, color='r', linewidth=0.5)
    meshax.set_xlabel('X')
    meshax.set_ylabel('Y')
    meshax.set_aspect('equal', 'box')
    meshax.set_title(f'Undeformed and Deformed Meshes: Mesh {suffixes[meshnum]}, Deformation Scaling of {scale}')
    meshax.legend(loc='upper right', shadow=True, fontsize='medium')

# Plot the x and y axis stresses
marks = ['x', '.', '+', '*']
colors = ['red', 'blue', 'green', 'black']
labels = ['6', '12', '24', 'R']

xaxSxxfig, xaxSxxax = plt.subplots()
xaxSyyfig, xaxSyyax = plt.subplots()
yaxSxxfig, yaxSxxax = plt.subplots()
yaxSyyfig, yaxSyyax = plt.subplots()

for idx in range(len(suffixes)):
    zxSxx = np.polyfit(xaxpos[idx][0:2], xaxSxx[idx][0:2], 1)
    zxSyy = np.polyfit(xaxpos[idx][0:2], xaxSyy[idx][0:2], 1)
    zySxx = np.polyfit(yaxpos[idx][0:2], yaxSxx[idx][0:2], 1)
    zySyy = np.polyfit(yaxpos[idx][0:2], yaxSyy[idx][0:2], 1)
    print(f'The xx-stress approximated by mesh {suffixes[idx]} at X/R = 1 is: {np.polyval(zxSxx, 1)}')
    print(f'The yy-stress approximated by mesh {suffixes[idx]} at Y/R = 1 is: {np.polyval(zySyy, 1)}')
    print(f'The yy-stress approximated by mesh {suffixes[idx]} at X/R = 1 is: {np.polyval(zxSyy, 1)}')
    print(f'The xx-stress approximated by mesh {suffixes[idx]} at Y/R = 1 is: {np.polyval(zySxx, 1)}')
    xaxSxxax.scatter(xaxpos[idx], xaxSxx[idx], marker=marks[idx], color=colors[idx], label=labels[idx])
    xaxSyyax.scatter(xaxpos[idx], xaxSyy[idx], marker=marks[idx], color=colors[idx], label=labels[idx])

    yaxSxxax.scatter(yaxpos[idx], yaxSxx[idx], marker=marks[idx], color=colors[idx], label=labels[idx])
    yaxSyyax.scatter(yaxpos[idx], yaxSyy[idx], marker=marks[idx], color=colors[idx], label=labels[idx])

xaxSxxax.set_xlabel('X/R')
xaxSxxax.set_ylabel(r'$\sigma_{xx} / \sigma$')
xaxSxxax.set_title(r'$\sigma_{xx}$ Along Mesh X-Axis')
xaxSxxax.set_aspect('equal', 'box')
xaxSxxax.legend(loc='upper right', shadow=True, fontsize='large')

xaxSyyax.set_xlabel('X/R')
xaxSyyax.set_ylabel(r'$\sigma_{yy} / \sigma$')
xaxSyyax.set_title(r'$\sigma_{yy}$ Along Mesh X-Axis')
xaxSyyax.legend(loc='upper right', shadow=True, fontsize='large')
xaxSyyax.set_aspect('equal', 'box')

yaxSxxax.set_xlabel('Y/R')
yaxSxxax.set_ylabel(r'$\sigma_{xx} / \sigma$')
yaxSxxax.set_title(r'$\sigma_{xx}$ Along Mesh Y-Axis')
yaxSxxax.set_aspect('equal', 'box')
yaxSxxax.legend(loc='upper right', shadow=True, fontsize='large')

yaxSyyax.set_xlabel('Y/R')
yaxSyyax.set_ylabel(r'$\sigma_{yy} / \sigma$')
yaxSyyax.set_title(r'$\sigma_{yy}$ Along Mesh Y-Axis')
yaxSyyax.set_aspect('equal', 'box')
yaxSyyax.legend(loc='upper right', shadow=True, fontsize='large')

plt.show()
