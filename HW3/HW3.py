# Comp. Methods for Struc. A. HW #3:
# Dewey Melton, dtm862
# Basic 2-D truss solver for non-dimensional members - does not account for a member's
# Young's modulus or cross-sectional area

import numpy as np

# Load in nodes and nodal positions
nodes = np.loadtxt('testnodes', skiprows=1, ndmin=2)
nnodes = nodes.shape[0]
# print('Nodes:')
# print(nodes)

# Load in boundary conditions
disps = np.loadtxt('testdisplacements', skiprows=1, ndmin=2)
ndbcs = disps.shape[0]
# print('Disps:')
# print(disps)

# Load in elements
elements = np.loadtxt('testelements', skiprows=1, ndmin=2)
neles = elements.shape[0]
# print('Elements:')
# print(elements)

# Load in known external/applied forces
forces = np.loadtxt('testforces', skiprows=1, ndmin=2)
nforces = forces.shape[0]
# print('Forces:')
# print(forces)

# Create gcon array
gcon = np.empty([nnodes, 2], dtype=int)
for idx in range(nnodes):
    gcon[idx, 0] = 2*idx
    gcon[idx, 1] = 2*idx + 1

# Apply known disps and rearrange these to bottom of gcon
for bc in disps.tolist():
    # Read in the node # and dof # of the boundary condition
    dbcnode = int(bc[0])
    dbcdof = int(bc[1])

    # Cycle through every element in gcon; move all other entries forwards
    dofi = gcon[dbcnode, dbcdof]  # Get gcon address of current bc
    for idx1 in range(nnodes):  # do i
        for idx2 in range(2):  # do j
            dofj = gcon[idx1, idx2]  # Get gcon address of the node dof currently being considered
            if dofj > dofi:  # If considered node dof gcon address greater than the address of bc, move it up one
                gcon[idx1, idx2] -= 1

    # Move current bc gcon to end
    gcon[dbcnode, dbcdof] = int(2*nnodes - 1)  # Psuedo code asks for 2*nnodes; include the -1 to correct for 0 indexing

# Calculate the truss' number of degrees of freedom
ndofs = 2*nnodes - ndbcs

# Initialize global force and displacement vectors
uglob = np.zeros([nnodes*ndofs, 1])
fglob = np.zeros([nnodes*ndofs, 1])
for bc in disps:
    uglob[gcon[int(bc[0]), int(bc[1])]] = bc[2]
for f in forces:
    fglob[gcon[int(f[0]), int(f[1])]] = f[2]

# Initialize reduced stiffness matrix and force vector
Kred = np.zeros([ndofs, ndofs])
Fred = fglob[0:ndofs, [0]]

# Generate actual values for reduced stiffness matrix and force vector
Lvec = np.empty(neles)
cvec = np.empty_like(Lvec)
svec = np.empty_like(Lvec)
for idx, ele in enumerate(elements.tolist()):
    x1 = nodes[int(ele[1]), 1]
    x2 = nodes[int(ele[2]), 1]
    y1 = nodes[int(ele[1]), 2]
    y2 = nodes[int(ele[2]), 2]
    dx, dy = x2 - x1, y2-y1
    L = np.sqrt(dx**2 + dy**2)
    c, s = dx/L, dy/L
    Lvec[idx], cvec[idx], svec[idx] = L, c, s
    Kloc = 1/L*np.array([[c**2, s*c, -c**2, -s*c],
                     [s*c, s**2, -s*c, -s**2],
                     [-c**2, -s*c, c**2, s*c],
                     [-s*c, -s**2, s*c, s**2]])
    for inode in range(2):
        for idof in range(2):
            ldofi = 2*inode + idof
            gdofi = gcon[int(ele[inode + 1]), idof]
            if gdofi >= ndofs:
                continue
            for jnode in range(2):
                for jdof in range(2):
                    ldofj = 2*jnode + jdof
                    gdofj = gcon[int(ele[jnode + 1]), jdof]
                    if gdofj >= ndofs:
                        Fred[gdofi, [0]] -= Kloc[ldofi, ldofj]*uglob[gdofj]
                    else:
                        Kred[gdofi, gdofj] += Kloc[ldofi, ldofj]

# Solve for the reduced displacement vector
ured = np.linalg.inv(Kred)@Fred

# Reconstruct global displacement vector
uglob[0:ndofs] = ured
u = np.zeros([nnodes, 2])
for idx1 in range(nnodes):
    for idx2 in range(2):
        u[idx1, idx2] = uglob[gcon[idx1, idx2]]
        if idx2 == 0:
            dofname = '(x-direction)'
        else:
            dofname = '(y-direction)'
        if u[idx1, idx2] == 0:
            unit = ''
        else:
            unit = 'PL/EA'
        print(f'The displacement of node {idx1} in d.o.f. {idx2} {dofname} is {u[idx1, idx2]:.3f} {unit}')

print(Kred)